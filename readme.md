This small project is a scratchpad for trying out the Longformer transformer on  Chenxin-Guo's law text classification. 

The code is based on https://github.com/Chenxin-Guo/number-identify/tree/master/Text_Classification and https://huggingface.co/transformers/model_doc/longformer.html. 

The domain is interesting because it has a manageable, but sizeable sample set suitable for multiclass classification. 

The Longfomer is a recent attempt at overcoming Bert's (and its derivatives) built-in limit on input text size (512). The paper is "Longformer: The Long-Document Transformer" by Beltagy et al (https://arxiv.org/pdf/2004.05150.pdf). Huggingfaces among others are currently working on an implementation (https://huggingface.co/transformers/model_doc/longformer.html).

The code herein is simply `longformer.py` 

GPU memory is the main limiting factor w.r.t batch size and/or text length. 

The code is sloppy on many levels, particularly w.r.t metrics and the use of attention as outlined in the article. 

# Experiments 

The experiments were done on an azure dsvm with a Tesla K80 with 11.17 Gb memory; 

```
+-----------------------------------------------------------------------------+
| NVIDIA-SMI 450.36.06    Driver Version: 450.36.06    CUDA Version: 11.0     |
|-------------------------------+----------------------+----------------------+
| GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
|                               |                      |               MIG M. |
|===============================+======================+======================|
|   0  Tesla K80           On   | 0000XXXX:00:00.0 Off |                    0 |
| N/A   54C    P0    75W / 149W |   7043MiB / 11441MiB |      0%      Default |
|                               |                      |                  N/A |
+-------------------------------+----------------------+----------------------+
```

chunk length 2048, batch size 1, 10% sample:

Cuda OOM error. 

chunk length 1792, batch size 1, 10% sample: 
```
Stage 7, step 186, batch length 3
Stage 7, step 187, batch length 3
Stage 7, step 188, batch length 3
done. avg train loss 2.9484135915362644
```



chunk length 1024, batch size 1, 10% sample: 
```
Stage 7, step 325, batch length 3
Stage 7, step 326, batch length 3
Stage 7, step 327, batch length 3
Stage 7, step 328, batch length 3
done. avg train loss 2.7054318506666957
```

