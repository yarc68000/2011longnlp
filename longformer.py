import re
import glob
import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from keras.preprocessing.sequence import pad_sequences
from torch.utils.data import TensorDataset, DataLoader, RandomSampler, SequentialSampler
from transformers import LongformerTokenizer, LongformerForSequenceClassification, AdamW
import torch

# cf. https://huggingface.co/transformers/model_doc/longformer.html,
# https://github.com/allenai/longformer, https://github.com/allenai/longformer/issues/80,
# https://discuss.pytorch.org/t/getting-cuda-out-of-memory-while-running-longformer-model-in-google-colab-similar-code-using-bert-is-working-fine/98068,

device = 'cuda'

print("stage 1")

# Read the txt into a dataframe with filename and corpus two columns
file_list = glob.glob("./Fixed_Judgements/*.txt")
corpus_dict = {'filename':[], 'corpus':[]}
for file_path in file_list:
    with open(file_path, 'rb') as f_input:
        f = f_input.read().decode('utf-8', errors='ignore')
    f.find('Judgment')
    f = f[f.find('Judgment')+9:]
    f = re.sub(r"\(\w+\)", "", f)
    f = re.sub(r"\(\w+\s+\)", "", f)
    f = re.sub(r"\<\/\w+\>", "", f)
    f = re.sub(r"\<\w+\>", "", f)
    f = re.sub(r"[A-Z]\.", "", f)
    f = re.sub(r"\((i|ii|iii|iv|v|vi|vii|viii|ix|x|xii|ia)\)", "", f)
    f = re.sub(r"\d+", "", f)

    corpus_dict['filename'].append(os.path.basename(file_path).split('.')[0])
    corpus_dict['corpus'].append(f)
corpuse = pd.DataFrame(corpus_dict)
corpuse.set_index('filename', inplace=True)
corpuse.head()

df = pd.read_csv("./Interview_Mapping.csv")
df['test'] = df['Area.of.Law'].map(lambda x: x == 'To be Tested')
df.set_index('Judgements', inplace=True)
df = df.merge(corpuse, how='left', left_index=True, right_index=True)

# only use a fraction of the sample, for dev purposes
df = df.sample(frac=0.1, random_state=1)

print("stage 2")

tokenizer = LongformerTokenizer.from_pretrained('allenai/longformer-base-4096')
df['tokens'] = [tokenizer.encode(sent, add_special_tokens = True) for sent in df['corpus']] # Add '[CLS]' and '[SEP]')
df.head()

f = plt.figure()
p1 = f.add_subplot(211)
# f, p1 = plt.subplots()

ls = df["tokens"].apply(lambda x: len(x))
x = list(range(1, len(ls)+1))
y = list(ls.sort_values(ascending=False))
p1.bar(x, y)
p1.title.set_text("Sample length")

x2 = pd.DataFrame(df[df["test"] == False].groupby(["Area.of.Law"]).size())
p2 = f.add_subplot(212)
x2.plot(kind='bar', ax=p2, legend=None)
p2.title.set_text("Category frequencies")


plt.show()
new_df = {"Judgements":[], "trunc_tokens":[]}

MAX_LEN = 1024 + 768
EFF_LEN = MAX_LEN - 2

for index, row in df.iterrows():
    NO_101102_token = row['tokens'][1:-1]
    if len(NO_101102_token)>EFF_LEN:
        for k in range(0, (len(NO_101102_token)//EFF_LEN)+1):
            new_df['Judgements'].append(index)
            new_tokens = [101]+ row['tokens'][EFF_LEN*(k):  EFF_LEN*(k+1)]+[102]
            new_df['trunc_tokens'].append(new_tokens)
new_df = pd.DataFrame(new_df).set_index('Judgements')
# now the length is 512 with 101 and 102 at the begining and the end
new_df = new_df.merge(df, how='left', right_index = True, left_index = True)
new_df.drop(['corpus',  'tokens'], axis = 1,  inplace = True)
print(new_df.shape)

print("stage 3")

labelencoder = LabelEncoder()
new_df['label'] = labelencoder.fit_transform(new_df['Area.of.Law'])
new_df.head()

input_ids = []
labels = []
test_ids = []
test_labels = []

for i in range(new_df.shape[0]):
    if new_df.iloc[i]['test'] == False:
        input_ids.append(new_df.iloc[i]['trunc_tokens'])
        labels.append(new_df.iloc[i]['label'])
    else:
        test_ids.append(new_df.iloc[i]['trunc_tokens'])
        test_labels.append(new_df.iloc[i]['label'])

input_ids = pad_sequences(input_ids, maxlen=MAX_LEN, dtype="long", value=0, truncating="post", padding="post")
test_ids = pad_sequences(test_ids, maxlen=MAX_LEN, dtype="long", value=0, truncating="post", padding="post")

all_labels = list(set(labels + test_labels))

attention_masks = []
for sent in input_ids:
    att_mask = [int(token_id > 0) for token_id in sent]
    attention_masks.append(att_mask)

print("stage 4")

train_inputs, validation_inputs, train_labels, validation_labels = train_test_split(input_ids, labels, random_state=2018, test_size=0.2)
# Do the same for the masks.
train_masks, validation_masks, _, _ = train_test_split(attention_masks, labels, random_state=2018, test_size=0.2)

train_inputs = torch.tensor(train_inputs)
validation_inputs = torch.tensor(validation_inputs)

train_labels = torch.tensor(train_labels)
validation_labels = torch.tensor(validation_labels)

train_masks = torch.tensor(train_masks)
validation_masks = torch.tensor(validation_masks)

batch_size = 1

# Create the DataLoader for our training set.
train_data = TensorDataset(train_inputs, train_masks, train_labels)
train_sampler = RandomSampler(train_data)
train_dataloader = DataLoader(train_data, sampler=train_sampler, batch_size=batch_size)

# Create the DataLoader for our validation set.
validation_data = TensorDataset(validation_inputs, validation_masks, validation_labels)
validation_sampler = SequentialSampler(validation_data)
validation_dataloader = DataLoader(validation_data, sampler=validation_sampler, batch_size=batch_size)


print("stage 5")

model = LongformerForSequenceClassification.from_pretrained('allenai/longformer-base-4096',
                                                            num_labels = 42,
                                                            return_dict=True,
                                                            output_attentions=False,
                                                            # Whether the model returns attentions weights.
                                                            output_hidden_states=False,
                                                            # Whether the model returns all hidden-states.
                                                            )

optimizer = AdamW(model.parameters(),
                  lr = 2e-5, # args.learning_rate - default is 5e-5, our notebook had 2e-5
                  eps = 1e-8 # args.adam_epsilon  - default is 1e-8.
                )

def flat_accuracy(preds, labels):
    pred_flat = np.argmax(preds, axis=1).flatten()
    labels_flat = labels.flatten()
    return np.sum(pred_flat == labels_flat) / len(labels_flat)

import random
seed_val = 42

random.seed(seed_val)
np.random.seed(seed_val)
torch.manual_seed(seed_val)
torch.cuda.manual_seed_all(seed_val)


total_loss = 0

print("stage 6")

model.cuda()
model.train()
for step, batch in enumerate(train_dataloader):
        print("Stage 7, step {}, batch length {}".format(step, len(batch)))
        b_input_ids = torch.as_tensor(batch[0], device=torch.device(device))
        b_input_mask = torch.as_tensor(batch[1], device=torch.device(device))
        b_labels = torch.as_tensor(batch[2], device=torch.device(device))
        model.zero_grad()
        outputs = model(b_input_ids,
                    token_type_ids=None,
                    attention_mask=b_input_mask,
                    labels=b_labels)
        loss = outputs[0]
        total_loss += float(loss.item())
        loss.backward()
        torch.nn.utils.clip_grad_norm_(model.parameters(), 1.0)
        optimizer.step()

    # Calculate the average loss over the training data.
avg_train_loss = total_loss / len(train_dataloader)

print("done. avg train loss {}".format(avg_train_loss))


